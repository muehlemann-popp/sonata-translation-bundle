<?php

namespace Ibrows\SonataTranslationBundle\Command;

use Lexik\Bundle\TranslationBundle\Translation\Translator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ClearCacheCommand
 * @package Ibrows\SonataTranslationBundle\Command
 */
class ClearCacheCommand extends Command
{
    protected static $defaultName = 'ibrows:sonatatranslationbundle:clearcache';

    /** @var ContainerInterface */
    protected $container;

    /** @var Translator */
    protected $translator;

    public function __construct(ContainerInterface $container, TranslatorInterface $translator)
    {
        $this->container = $container;
        $this->translator = $translator;

        parent::__construct();
    }

    public function configure()
    {
        $this
            ->setDescription('Clears the translation cache')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $locales = $this->getManagedLocales();
        $output->writeln('Remove cache for translations in: '. implode(", ", $locales));
        $this->translator->removeLocalesCacheFiles($locales);
    }

    /**
     * @return array
     */
    protected function getManagedLocales()
    {
        return $this->container->getParameter('lexik_translation.managed_locales');
    }
}
